package com.test.myapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.test.myapplication.app.Constant;
import com.test.myapplication.app.GlobalHandler;
import com.test.myapplication.utils.LongClickUtil;
import com.test.myapplication.utils.SharePUtil;
import com.test.myapplication.utils.ToastUtil;

import java.util.HashMap;


public class WebViewActivity extends AppCompatActivity {

    private WebView mWebView;
    private WebSettings settings;
    private String s_ip,s_port,s_hp,URL;
    private GlobalHandler handler;
    private int type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_web_view);

        getWindow().setStatusBarColor(Color.TRANSPARENT);
        // 在调用TBS初始化、创建WebView之前进行如下配置
        HashMap map = new HashMap();
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        QbSdk.initTbsSettings(map);
        Intent intent = getIntent();
        type = intent.getIntExtra("type",1);
        s_hp = intent.getStringExtra(Constant.HTTP);
        s_ip = intent.getStringExtra(Constant.IP);
        s_port = intent.getStringExtra(Constant.PORT);
        initView();
        initWebSetting(mWebView);
    }

    @SuppressLint({"ClickableViewAccessibility", "SetJavaScriptEnabled"})
    private void initWebSetting(WebView webView) {
        settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        settings.setDisplayZoomControls(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setBlockNetworkImage(false);
        settings.setLoadsImagesAutomatically(true);
        settings.setDefaultTextEncodingName("utf-8");
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); //设置缓存
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        /* 设置缓存模式 */
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        /* 设置为true表示支持使用js打开新的窗口 */
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        /* 大部分网页需要自己保存一些数据,这个时候就的设置下面这个属性 */
        settings.setDomStorageEnabled(true);
        /* 设置为使用webView推荐的窗口 */
        settings.setUseWideViewPort(true);
        /* 设置网页自适应屏幕大小 ---这个属性应该是跟上面一个属性一起用 */
        settings.setLoadWithOverviewMode(true);
        /* HTML5的地理位置服务,设置为true,启用地理定位 */
        settings.setGeolocationEnabled(true);
        /* 设置是否允许webView使用缩放的功能,我这里设为false,不允许 */
        settings.setBuiltInZoomControls(false);
        /* 提高网页渲染的优先级 */
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        /* 设置显示水平滚动条,网页右边的滚动条*/
        webView.setHorizontalScrollBarEnabled(false);
        //设置true,才能让WebView支持<meta>标签的viewport属性
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        //设置可以支持缩放
        settings.setSupportZoom(true);
        //设置出现缩放工具
        settings.setBuiltInZoomControls(true);
        //设定缩放控件隐藏
        settings.setDisplayZoomControls(false);
        //当访问https网页时，允许加载http图片，默认不能混合加载
        webView.setWebChromeClient(new WebChromeClient());//这行最好不要丢掉
        //该方法解决的问题是打开浏览器不调用系统浏览器，直接用webView打开
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.isEmpty())
                    return false;
                if (url.startsWith("http") || url.startsWith("https"))
                    return false;
                try {
                    if (url.startsWith("weixin://") || url.startsWith("alipays://") || url.startsWith("mailto://")
                            || url.startsWith("tel://") || url.startsWith("baidu") || url.startsWith("dianping://")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
                System.out.println("tag"+webResourceResponse.getStatusCode());
                if (webResourceResponse.getStatusCode() == 400) {
                    SharePUtil.getInstance().saveData(Constant.USE_SAVE,true);

                }
            }

            @Override
            public void onReceivedError(WebView webView, int i, String s, String s1) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    SharePUtil.getInstance().saveData(Constant.USE_SAVE,true);
                    Log.d("TAG", "onReceivedError: "+s);
                } else {
                    SharePUtil.getInstance().saveData(Constant.USE_SAVE,true);
                    ToastUtil.showMsg("格式错误");
                }

                Log.d("TAG", "onReceivedError: "+s);
            }
        });
        webView.resumeTimers();
    }

    private void initView() {
        handler = GlobalHandler.getInstance();
        mWebView = (WebView) findViewById(R.id.webView);
        URL = s_hp + "://"+ s_ip + ":" + s_port+ "/"+Constant.URL_PR;
        URL = URL.replace(" ","");

        System.out.println(URL);
        loadUrl(URL);
        //mWebView.loadUrl(URL);

        //屏幕长按返回
        LongClickUtil.setLongClick(handler, mWebView, 3000, new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(WebViewActivity.this,MainActivity.class);
                SharePUtil.getInstance().saveData(Constant.USE_SAVE,true);
                startActivity(intent);
                finish();
                ToastUtil.showMsg("退出");
                return true;
            }
        });
    }


    private void loadUrl(String url) {
        if (mWebView !=null) {
            mWebView.clearCache(true);
            mWebView.clearHistory();
            mWebView.loadUrl(URL);
            mWebView.resumeTimers();
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
        mWebView.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
        mWebView.getSettings().setLightTouchEnabled(false);
    }

    //销毁 放置内存泄漏
    @Override
    public void onDestroy() {
        if (this.mWebView != null) {
            mWebView.destroy();
        }
        super.onDestroy();
    }
}
