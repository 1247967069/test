package com.test.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.test.myapplication.app.Constant;
import com.test.myapplication.utils.ActivityUtil;
import com.test.myapplication.utils.SharePUtil;
import com.test.myapplication.utils.ToastUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Intent intent;
    private TextView tv_ip,tv_port;
    private int type;//0为http，1为https
    private EditText ed_ip,ed_port;
    private Button btn_in;
    private String t_ip,t_port,t_hp;
    private Object USE_F;
    private Spinner sp_hp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setStatusBarColor(Color.TRANSPARENT);
        ActivityUtil.globalWindow = getWindow();//获取全局window
        ActivityUtil.setBottomNavbarTransparent();//设置华为手机底部透明
        intent = new Intent(this,WebViewActivity.class);
        initView();
    }


    private void initView() {
        tv_ip = (TextView) findViewById(R.id.tv_ip);
        tv_port = (TextView) findViewById(R.id.tv_port);
        sp_hp = (Spinner) findViewById(R.id.se_hp);
        ed_ip = (EditText) findViewById(R.id.et_ip);
        ed_port = (EditText) findViewById(R.id.et_port);
        btn_in = (Button) findViewById(R.id.btn_in);
        btn_in.setOnClickListener(this);
        sp_hp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String info = (String) sp_hp.getSelectedItem();
                if (info.equals("http")) {
                    type = 0;
                    tv_port.setVisibility(View.VISIBLE);
                    ed_port.setVisibility(View.VISIBLE);
                    intent.putExtra("type",0);
                } else if (info.equals("https")){
                    type = 1;
                    intent.putExtra("type",1);
                    SharePUtil.getInstance().saveData(Constant.USE_PORT,"443");
                    tv_ip.setText("服务器IP/端口");
                    tv_port.setVisibility(View.GONE);
                    ed_port.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //服务器ip跟端口
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_in:

                t_hp = sp_hp.getSelectedItem().toString().trim();

                t_ip = ed_ip.getText().toString().trim();
                t_port = ed_port.getText().toString().trim();

                if (t_ip.isEmpty()) {
                    ToastUtil.showMsg("输入项为空");
                } else {
                    if (type==1) {
                        SharePUtil.getInstance().saveData(Constant.USE_SAVE,false);
                        SharePUtil.getInstance().saveData(Constant.USE_HTTP,t_hp);
                        SharePUtil.getInstance().saveData(Constant.USE_IP,t_ip);
                        SharePUtil.getInstance().saveData(Constant.USE_PORT,"443");
                        t_port = "443";
                        startWebView();
                    } else if (!t_port.isEmpty()){
                        SharePUtil.getInstance().saveData(Constant.USE_SAVE,false);
                        SharePUtil.getInstance().saveData(Constant.USE_HTTP,t_hp);
                        SharePUtil.getInstance().saveData(Constant.USE_IP,t_ip);
                        SharePUtil.getInstance().saveData(Constant.USE_PORT,t_port);
                        startWebView();
                    } else {
                        ToastUtil.showMsg("输入项为空");
                    }
                }
                break;

        }
    }

    private void startWebView() {
        intent.putExtra(Constant.HTTP,t_hp);
        intent.putExtra(Constant.IP,t_ip);
        intent.putExtra(Constant.PORT,t_port);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        USE_F = SharePUtil.getInstance().outData(Constant.USE_SAVE,true);
        if ((boolean)USE_F) {

        } else {
            t_hp = (String) SharePUtil.getInstance().outData(Constant.USE_HTTP,"http");
            t_ip = (String) SharePUtil.getInstance().outData(Constant.USE_IP,"ip");
            t_port = (String) SharePUtil.getInstance().outData(Constant.USE_PORT,"port");
            intent.putExtra(Constant.HTTP,t_hp);
            intent.putExtra(Constant.IP,t_ip);
            intent.putExtra(Constant.PORT,t_port);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
            startActivity(intent);
        }
        super.onStart();
    }
}