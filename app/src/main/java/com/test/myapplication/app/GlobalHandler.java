package com.test.myapplication.app;

import android.os.Handler;


public class GlobalHandler extends Handler {

    /**
     * 全局handler防止泄漏
     */
    private GlobalHandler() {

    }

    private static class Holder {
        private static final GlobalHandler HANDLER = new GlobalHandler();
    }

    public static GlobalHandler getInstance() {
        return Holder.HANDLER;
    }
}
