package com.test.myapplication.app;

import com.test.myapplication.utils.SharePUtil;

public class Constant {
    /**
     * Key关键字(intent,shareP)
     */
    //mWebView.loadUrl(" https://publish.tengeredu.com:443/h5/login");
    public final static String HTTP = "http";
    public final static String IP = "ip";
    public final static String PORT = "port";

    public final static String USE_SAVE = "use_save";
    public final static String USE_HTTP = "use_http";
    public final static String USE_IP = "use_ip";
    public final static String USE_PORT = "use_port";

    /**
     * url相关
     */
    public final static String URL_RE = "reception";//后台接收
    public final static String URL_PR = "provider";//前台提供
}
