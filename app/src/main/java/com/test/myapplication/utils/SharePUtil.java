package com.test.myapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class SharePUtil {

    public static final String FILE_NAME = "FILE_SAVE";

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private static SharePUtil mSharePUtil;


    //构造方法
    private SharePUtil() {}

    public void init(Context context){
        sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
    }


    //单例模式
    public static SharePUtil getInstance() {
        if (mSharePUtil == null) {
            synchronized (SharePUtil.class) {
                if(mSharePUtil == null)
                    mSharePUtil = new SharePUtil();
            }
        }
        return mSharePUtil;
    }

    /**
     * 保存数据
     */
    public  void saveData(String key, Object obj) {
        if (obj instanceof Boolean) {
            editor.putBoolean(key, (Boolean) obj);

        } else if (obj instanceof Float) {
            editor.putFloat(key, (Float) obj);

        } else if (obj instanceof Integer) {
            editor.putInt(key, (Integer) obj);

        } else if (obj instanceof Long) {
            editor.putLong(key, (Long) obj);

        } else {
            editor.putString(key, (String) obj);
        }
        editor.commit();
    }

    /**
     * 取出数据
     */

    public  Object outData(String key,Object object) {
        if (object instanceof  Boolean) {
            return sp.getBoolean(key,(boolean) object);

        } else if (object instanceof  Float) {
            return sp.getFloat(key,(Float) object);

        } else if (object instanceof Integer) {
            return sp.getInt(key,(Integer) object);

        } else if (object instanceof Long) {
            return sp.getLong(key, (Long) object);

        } else if (object instanceof String){
            return sp.getString(key,(String) object);
        }
        return null;
    }

    /**
     * 删除指定数据
     */
    public   void delData(Context context,String key) {
        editor.remove(key);
        editor.commit();
    }

    /**
     * 删除所有数据
     */
    public  void delAllData(Context context,String key) {
        editor.clear();
        editor.commit();
    }

    /**
     * 检查key对应的数据是否存在
     */
    public  boolean contains(String key) {
        return sp.contains(key);
    }

}
