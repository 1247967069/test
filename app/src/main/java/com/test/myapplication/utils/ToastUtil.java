package com.test.myapplication.utils;

import android.content.Context;
import android.widget.Toast;

import com.test.myapplication.app.MyApplication;

public class ToastUtil {
    private static Toast mToast;

    public static void showMsg(String text) {
        synchronized (ToastUtil.class) {
            if (mToast != null) {
                mToast.cancel();
                mToast = null;
            }
            Context context = MyApplication.getInstance();
            Toast.makeText(context,text,Toast.LENGTH_SHORT).show();
        }

    }
}
