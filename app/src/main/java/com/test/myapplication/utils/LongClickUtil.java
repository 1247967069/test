package com.test.myapplication.utils;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

/**
 * 自定义长按事件
 */
public class LongClickUtil {

    /**
     *
     * @param handler 外界handler(为了减少handler的泛滥使用,最好全局传handler引用,如果没有就直接传 new Handler())
     * @param longClickView 触发事件的组件
     * @param delayMillis 触发时间
     * @param longClickListener 监听事件
     */
    public static void setLongClick(final Handler handler, final View longClickView, final long delayMillis, final View.OnLongClickListener longClickListener) {

        longClickView.setOnTouchListener(new View.OnTouchListener() {


            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        //抬起，移除当前Runnable回调
                        handler.removeCallbacks(r);
                        System.out.println("点击了ACTION_UP事件");
                        break;

                    case MotionEvent.ACTION_MOVE:
                        //在屏幕拖动是否需要重新计时
                        System.out.println("点击了ACTION_MOVE事件");
                        break;

                    case MotionEvent.ACTION_DOWN:
                        System.out.println("点击了ACTION_DOWN事件");
                        // 每次按下重新计时
                        // 按下前,先移除 已有的Runnable回调,防止用户多次单击导致多次回调长按事件的bug
                        handler.removeCallbacks(r);
                        // 按下时,开始计时
                        handler.postDelayed(r, delayMillis);
                        break;

                }
                //false可以继续监听触摸事件，true反之
                return false;

            }

            private Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (longClickListener != null) {
                        longClickListener.onLongClick(longClickView);
                    }
                }
            };
        });

    }
}
