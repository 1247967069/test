package com.test.myapplication.utils;

import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

/**
 * 针对布局的工具
 */
public class ActivityUtil {

    /**
     * 全局window
     */
    public static Window globalWindow;

    /**
     * 设置华为手机底部导航栏透明
     */
    public static void setBottomNavbarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (globalWindow != null) {//FLAG_TRANSLUCENT_NAVIGATION // FLAG_TRANSLUCENT_STATUS
                globalWindow.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            }
        }
    }
}